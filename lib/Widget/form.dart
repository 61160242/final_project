
import 'package:caffeien/sleep.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../main.dart';
import '../sleep_service.dart';

// void main(List<String> args) {
//   runApp(MyForm());
// }

class MyForm extends StatelessWidget {
  const MyForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF6D4C41),
          title: Center(
            child: Text('Sleep'),
          ),
        ),
        body: MySleep(),
      ),
    );
  }
}

class MySleep extends StatefulWidget {
  MySleep({Key? key}) : super(key: key);

  @override
  _MySleepState createState() => _MySleepState();
}

class _MySleepState extends State<MySleep> {
  int val = -1;
  final _formKey = GlobalKey<FormState>();
  final dateFormat = DateFormat("yyyy-MM-dd");
  final timeFormat = DateFormat("HH:mm");
  String date = "";
  String bedtime = "";
  String wakeup = "";
  ListSleep listsleep =
      new ListSleep(id: -1, date: '', bedtime: '', wakeup: '', status: '');

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        padding:
            EdgeInsets.only(left: 40.0, right: 40.0, bottom: 20.0, top: 50.0),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _formKey,
          child: Column(
            children: [
              //Date
              DateTimePicker(
                type: DateTimePickerType.date,
                dateMask: 'yyyy-MM-dd',
                firstDate: DateTime(2000),
                lastDate: DateTime(2100),
                dateLabelText: 'Date',
                locale: Locale('en', 'US'),
                onChanged: (value) {
                  setState(() {
                    listsleep.date = value;
                  });
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please select date';
                  }
                  return null;
                },
              ),
              Container(
                padding: EdgeInsets.only(
                    left: 40.0, right: 40.0, bottom: 20.0, top: 20.0),
              ),
              //BedTime
              DateTimePicker(
                type: DateTimePickerType.time,
                timePickerEntryModeInput: true,
                timeLabelText: 'Bedtime',
                use24HourFormat: true,
                locale: Locale('en', 'US'),
                onChanged: (value) {
                  setState(() {
                    listsleep.bedtime = value;
                  });
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please select time';
                  }
                  return null;
                },
              ),
              Container(
                padding: EdgeInsets.only(
                    left: 40.0, right: 40.0, bottom: 20.0, top: 20.0),
              ),
              //WakeUp
              DateTimePicker(
                type: DateTimePickerType.time,
                timePickerEntryModeInput: true,
                timeLabelText: 'Wake Up',
                use24HourFormat: true,
                locale: Locale('en', 'US'),
                onChanged: (value) {
                  setState(() {
                    listsleep.wakeup = value;
                  });
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please select time';
                  }
                  return null;
                },
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: 40.0, right: 40.0, bottom: 20.0, top: 20.0),
                  ),
                  Text('Do you sleep?'),
                  Container(
                    child: ListTile(
                      title: Text('sleep'),
                      leading: Radio(
                        value: 1,
                        groupValue: val,
                        onChanged: (value) {
                          setState(() {
                            val = value as int;
                          });
                        },
                        activeColor: Color(0XFFEED9CD),
                      ),
                    ),
                  ),
                ],
              ),
              ListTile(
                title: Text('can not sleep'),
                leading: Radio(
                  value: 2,
                  groupValue: val,
                  onChanged: (value) {
                    setState(() {
                      val = value as int;
                    });
                  },
                  activeColor: Color(0XFFEED9CD),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20.0),
                margin: EdgeInsets.all(20.0),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                      Color(0xFF6D4C41),
                    ),
                  ),
                  child: Text('SAVE'),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      await addNewList(listsleep);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MainMenu(
                                    title: 'CAFFEINE WITH ME',
                                    status: true,
                                    statusplus: true,
                                    total: 0,
                                  )));
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
