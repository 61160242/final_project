import 'package:caffeien/sleep_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

import '../sleep.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({Key? key, required this.total}) : super(key: key);
  double total;

  @override
  _HomeWidgetState createState() => _HomeWidgetState(this.total);
}

class _HomeWidgetState extends State<HomeWidget> {
  String _picUrl = '';
  String _name = '';
  double _caffStart = 400;
  double _caffEnd = 0;
  double total;
  late ListSleep _list;
  _HomeWidgetState(this.total);
  Map<String, double> map() {
    Map<String, double> _dataMap = {'full': _caffStart, 'less': _caffEnd};
    return _dataMap;
  }

  @override
  void initState() {
    super.initState();
    _picUrl = FirebaseAuth.instance.currentUser!.photoURL!;
    _name = FirebaseAuth.instance.currentUser!.displayName!;
    _caffStart = 400-total;
    _caffEnd=total;
    _list = getList();
  }

  Widget pieRing() {
    return PieChart(
      centerText: 'Goal \n 400/mg',
      chartType: ChartType.ring,
      dataMap: map(),
      chartRadius: 150,
      chartLegendSpacing: 32,
      chartValuesOptions: ChartValuesOptions(
          showChartValuesInPercentage: true,
          showChartValueBackground: false,
          showChartValuesOutside: true),
      legendOptions: LegendOptions(
          showLegendsInRow: false,
          legendTextStyle: TextStyle(fontWeight: FontWeight.bold)),
      emptyColor: Color(0XFFEED9CD),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          Card(
            child: Container(
              // padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Container(
                    color: Color(0XFFEED9CD),
                    margin: EdgeInsets.all(2.0),
                    padding: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.all(16.0),
                              alignment: Alignment.center,
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(_picUrl),
                                    fit: BoxFit.fill),
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.all(16.0),
                                child: Text(
                                  _name,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: Container(
              // padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Container(
                    color: Color(0XFFEED9CD),
                    margin: EdgeInsets.all(2.0),
                    padding: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.all(10.0),
                              child: Icon(Icons.coffee),
                            ),
                            Container(
                              margin: EdgeInsets.all(5.0),
                              child: Text(
                                'Caffeine/day',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        pieRing()
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: Container(
              // padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Container(
                    color: Color(0XFFEED9CD),
                    margin: EdgeInsets.all(2.0),
                    padding: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.all(10.0),
                              child: Icon(Icons.bedtime),
                            ),
                            Container(
                              margin: EdgeInsets.all(5.0),
                              child: Text(
                                'Sleep/day',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                Container(
                                  child: Text('Caffeine $total mg',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                ),
                                Container(
                                  child:(_list.status == 'sleep')?
                                      Text('I can sleep between ${_list.bedtime}-${_list.wakeup}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),):
                                      Text('I can not sleep between ${_list.bedtime}-${_list.wakeup}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16))
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
