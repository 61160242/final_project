import 'package:caffeien/Widget/home.dart';
import 'package:caffeien/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../drink.dart';
import '../drink_service.dart';

// void main(List<String> args) {
//   runApp(MaterialApp(
//     title: 'List',
//     home: MyList(),
//   ));
// }

class MyList extends StatefulWidget {
  MyList({Key? key}) : super(key: key);

  @override
  _MyListState createState() => _MyListState();
}

class _MyListState extends State<MyList> {
  var arr = [];
  double total = 0;
  int num = 0;
  late Future<List<ToList>> _lists;
  @override
  void initState() {
    super.initState();
    _lists = getToLists();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 25,
        ),
        Text('Item: $arr'),
        Text('Total:$total'),
        Expanded(
          child: FutureBuilder(
            future: _lists,
            builder: (context, snapshot) {
              List<ToList> lists = snapshot.data as List<ToList>;
              if (snapshot.hasError) {
                return Text('Error');
              }
              return Container(
                  padding: const EdgeInsets.all(20.0),
                  child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 500,
                              childAspectRatio: 3 / 2,
                              crossAxisSpacing: 20,
                              mainAxisSpacing: 20),
                      itemCount: lists.length,
                      itemBuilder: (context, index) {
                        var list = lists.elementAt(index);
                        return Column(
                          children: [
                            Container(
                              child: Card(
                                  color: Color(0XFFEED9CD),
                                  child: Container(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.all(16.0),
                                          child: Image.network(
                                            'https://firebasestorage.googleapis.com/v0/b/caffeine-1c4ad.appspot.com/o/images%2Famaricano.jpg?alt=media&token=955b20d4-38cc-4dbd-a8c3-7a77b392ee5e',
                                            width: 100,
                                            height: 100,
                                          ),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Text('Name: ${list.name}'),
                                        ),
                                        Text('Caffeine: ${list.caff} mg.')
                                      ],
                                    ),
                                  )),
                            ),
                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(5.0),
                                    child: ElevatedButton(
                                      child: Text('-'),
                                      onPressed: () {
                                        setState(() {
                                          // arr.remove(list.name);
                                          for (var item in arr) {
                                            if (item == list.name) {
                                              print(
                                                  'list: ${list.caff}, total:$total');
                                              total -= list.caff;
                                              break;
                                            }
                                          }
                                          arr.remove(list.name);
                                        });
                                        print('${list.caff}');
                                        print('$total');
                                      },
                                    ),
                                  ),
                                  ElevatedButton(
                                    child: Text('+'),
                                    onPressed: () {
                                      setState(() {
                                        arr.add(list.name);
                                        total += list.caff;
                                      });
                                      print('${list.caff}');
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      }));
            },
          ),
        ),
        ElevatedButton(
          child: Text('save'),
          onPressed: () {
            if(total>400){
              total = 400;
            }
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => MainMenu(status: false, title: 'CAFFEINE WITH ME', statusplus: false, total: total,)));
          },
        ),
      ],
    );
  }
}
