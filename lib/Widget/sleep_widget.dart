import 'package:caffeien/sleep_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import '../sleep.dart';

class ListTimeWidget extends StatefulWidget {
  ListTimeWidget({Key? key}) : super(key: key);

  @override
  _ListTimeWidgetState createState() => _ListTimeWidgetState();
}

class _ListTimeWidgetState extends State<ListTimeWidget> {
  late Future<List<ListSleep>> _sleeps;

  @override
  void initState() {
    super.initState();
    setState(() {
      _sleeps = getToSleepLists();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _sleeps,
      builder: (context, snapshot) {
        List<ListSleep> sleeps = snapshot.data as List<ListSleep>;
        if (snapshot.hasError) {
          return Text('Error');
        }
        return ListView.builder(
          itemBuilder: (context, index) {
            var list = sleeps.elementAt(index);
            return Container(
              child: ListTile(
                title: Text(list.date),
                subtitle: Text('${list.bedtime} - ${list.wakeup}'),
                onTap: () {},
              ),
            );
          },
          itemCount: sleeps.length,
        );
      },
    );
  }
}
