
class ToList {
  int listId;
  String name;
  String nameImg;
  int caff;
  bool status;

  ToList(
      {required this.listId,
      required this.name,
      required this.nameImg,
      required this.caff,
      this.status = false});

  Map<String, dynamic> toMap() {
    return {
      'id': listId,
      'name': name,
      'nameImg': nameImg,
      'caff': caff,
      'status': status,
    };
  }

  static List<ToList> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (index) {
      return ToList(
          listId: maps[index]['list_id'],
          name: maps[index]['name'],
          nameImg: maps[index]['name_img'],
          caff: maps[index]['caff'],
          status: maps[index]['status']);
    }).toList();
  }
  @override
  String toString() {
    return 'Tolist {$listId $name $nameImg $caff}';
  }
}
