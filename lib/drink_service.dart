import 'drink.dart';

var mockTolists = [
  ToList(listId: 1, name: "Amaricano", nameImg: "amaricano.png", caff: 150),
  ToList(listId: 2, name: "Green Tea", nameImg: "greentea.png", caff: 50),
  ToList(listId: 3, name: "Coco", nameImg: "amaricano.png", caff: 70),
  ToList(listId: 4, name: "Tea Thai", nameImg: "amaricano.png", caff: 20)
];

Future<List<ToList>> getToLists(){
  return Future.delayed(Duration(seconds: 1),()=> mockTolists);
}
