import 'dart:async';

import 'package:caffeien/sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

import 'Widget/form.dart';
import 'Widget/home.dart';
import 'Widget/list.dart';
import 'Widget/sleep_widget.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;

  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((user) {
      // print(event.toString());
      // print(event!.metadata.toString());
      _navigatorKey.currentState!.pushReplacementNamed(
        user != null ? 'home' : 'login',
      );
    });
  }

//cancel
  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [Locale('en', 'US')],
      title: 'HelloWorld',
      navigatorKey: _navigatorKey,
      initialRoute:
          FirebaseAuth.instance.currentUser == null ? 'login' : 'home',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case 'home':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => MainMenu(
                title: 'Main',
                status: false,
                statusplus: false,
                total: 0,
              ),
            );
          case 'login':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => LoginPage(),
            );
          default:
            return MaterialPageRoute(
              settings: settings,
              builder: (_) => UnknownPage(),
            );
        }
      },
    );
  }
}

class MainMenu extends StatefulWidget {
  MainMenu({
    Key? key,
    required this.title,
    required this.status,
    required this.statusplus,
    required this.total,
  }) : super(key: key);
  String title;
  bool status;
  bool statusplus;
  double total;
  @override
  _MainMenuState createState() =>
      _MainMenuState(this.title, this.status, this.statusplus,this.total);
}

class _MainMenuState extends State<MainMenu> {
  _MainMenuState(this.title, this.status, this.statusplus,this.total);
  bool statusplus;
  bool status;
  bool setpage = false;
  String title;
  double total;
  late Widget body;
  @override
  void initState() {
    body=Center(child: HomeWidget(total: total));
    super.initState();
    if (status) {
      body = ListTimeWidget();

      status = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF8D6E63),
        title: Center(
          child: Text('CAFFEINE WITH ME'),
        ),
      ),
      body: body,
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
                decoration: BoxDecoration(
                  color: Color(0xFF8D6E63),
                ),
                child: Center(
                  child: Text('CAFFEINE WITH ME'),
                )),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () {
                setState(() {
                  body = HomeWidget(total: total,);
                  setpage = false;
                  statusplus = false;
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.list),
              title: const Text('List'),
              onTap: () {
                setState(() {
                  body = MyList();
                  setpage = false;
                  statusplus = false;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.bedtime),
              title: const Text('Sleep'),
              onTap: () {
                setState(() {
                  body = ListTimeWidget();
                  setpage = true;
                  statusplus = false;
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: const Text('Log out'),
              onTap: () async {
                await FirebaseAuth.instance.signOut();
              },
            ),
          ],
        ),
      ),
      floatingActionButton: new Visibility(
        child: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            setState(() {
              body = MySleep();
              setpage = false;
              statusplus = false;
            });
          },
        ),
        visible: (setpage || statusplus) ? true : false,
      ),
    );
  }
}
