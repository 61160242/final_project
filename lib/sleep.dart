class ListSleep {
  int id;
  String date;
  String bedtime;
  String wakeup;
  String status;

  ListSleep(
      {required this.id,
      required this.date,
      required this.bedtime,
      required this.wakeup,
      required this.status});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'date': date,
      'bedtime': bedtime,
      'wakeup': wakeup,
      'status': status,
    };
  }

  static List<ListSleep> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (index) {
      return ListSleep(
          id: maps[index]['id'],
          date: maps[index]['date'],
          bedtime: maps[index]['bedtime'],
          wakeup: maps[index]['wakeup'],
          status: maps[index]['status']);
    }).toList();
  }

  @override
  String toString() {
    return 'Tolist {$id $date $bedtime $wakeup $status }';
  }
}
