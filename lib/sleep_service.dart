import 'sleep.dart';

int lastId = 4;
var mocklistSleeps = [
  ListSleep(
      id: 1,
      date: '2021-10-28',
      bedtime: '02:00',
      wakeup: '10.00',
      status: 'I can not sleep'),
  ListSleep(
      id: 2,
      date: '2021-10-27',
      bedtime: '20:30',
      wakeup: '09.00',
      status: 'sleep'),
  ListSleep(
      id: 3,
      date: '2021-10-26',
      bedtime: '00:50',
      wakeup: '12.00',
      status: 'sleep'),
  ListSleep(
      id: 4,
      date: '2021-10-25',
      bedtime: '22.00',
      wakeup: '11.00',
      status: 'I can not sleep'),
];
Future<List<ListSleep>> getToSleepLists() {
  return Future.delayed(Duration(seconds: 1), () => mocklistSleeps);
}

Future<void> addNewList(ListSleep s) {
  return Future.delayed(Duration(seconds: 1), () {
    print('id $lastId');
    mocklistSleeps.add(ListSleep(
        id: lastId++,
        date: s.date,
        bedtime: s.bedtime,
        wakeup: s.wakeup,
        status: s.status));
  });
}

ListSleep getList() {
  return mocklistSleeps[mocklistSleeps.length - 1];
}
